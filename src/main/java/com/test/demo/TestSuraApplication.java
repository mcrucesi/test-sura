package com.test.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSuraApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestSuraApplication.class, args);
	}
}