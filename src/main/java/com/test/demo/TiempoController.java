package com.test.demo;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

@RestController
public class TiempoController {
	
	public static int INDENTATION = 4;
	private static final String URL_API_TIEMPO = "http://api.tiempo.com/index.php?api_lang=es&localidad={$codigo}&affiliate_id=4z3v8xe4qwae";

	@RequestMapping(path = "/api/{codigo}")
	public String index(@PathVariable("codigo") String codigo) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			
			String urlReplace = URL_API_TIEMPO.replace("{$codigo}", codigo);
			
			ResponseEntity<String> responseEntity = restTemplate.getForEntity(urlReplace, String.class);
	        String xmlTiempo = responseEntity.getBody();
	        
	        try {
				JSONObject jsonPronostico = XML.toJSONObject(xmlTiempo);
				String jsonPronosticoTiempo = jsonPronostico.toString(INDENTATION);
	
				System.out.println("JSON::: "+ jsonPronosticoTiempo);
				
				return jsonPronosticoTiempo;
				
			} catch (JSONException ex) {
				throw new TestExceptions("{error:{errorCode: '2', errorDescripcion: 'Ciudad no encontrada'}}");
			}
		}
		catch(Exception ex) {
			throw new TestExceptions("{error:{errorCode: '1', errorDescripcion: 'Parametro faltante'}}");
		}
	}

}