package com.test.demo;

public class TestExceptions extends RuntimeException {

    public TestExceptions(String json) {
        super(json);
    }

}